# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2017"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import threading
import enum

import numpy as np


class Modes(enum.Enum):
    STREAM = 2
    SINGLE = 0

    
class ForceTorqueI(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.daemon = True
        self._ft_lock = threading.Lock()
        self._new_ft_cond = threading.Condition()
        self._bias = np.zeros(6)
        self._latest_ft = None
        self.__stop = False
        self._stream_flag = threading.Event()
        self._mode = Modes.SINGLE
        self.start()
    
    def get_ft(self, wait=False):
        if self._mode == Modes.SINGLE:
            self._req_single_ft()
        elif wait:
            self.wait_for_data()
        with self._ft_lock:
            return self._latest_ft - self._bias                  

    ft = property(get_ft)

    def zero_bias(self):
        with self._ft_lock:
            self._bias = np.zeros(6)

    def sample_bias(self, N=5):
        new_bias = np.zeros(6)
        for i in range(N):
            if self._mode == Modes.STREAM:
                self.wait_for_data()
            else:
                self._req_single_ft()
            new_bias += self._latest_ft
        with self._ft_lock:
            self._bias = new_bias / N

    def _recv_ft(self):
        return NotImplemented

    def _req_single_ft(self):
        return NotImplemented
    
    @property
    def is_streaming(self):
        return self._mode == Modes.STREAM

    @property
    def is_single(self):
        return self._mode == Modes.SINGLE

    def start_stream(self):
        self._mode = Modes.STREAM
        self._stream_flag.set()

    def stop_stream(self):
        self._stream_flag.clear()
        self._mode = Modes.SINGLE
        # Clear the receive buffer
        while self._latest_ft is not None:
            self._recv_ft()
        return True

    def wait_for_data(self):
        """Method for aligning on reception of new FT-data."""
        with self._new_ft_cond:
            self._new_ft_cond.wait()

    def run(self):
        while not self.__stop:
            self._stream_flag.wait()
            self._recv_ft()
            with self._new_ft_cond:
                self._new_ft_cond.notify_all()


from .ati_rdt import ATIRDT
from .optoforce import OptoForce
from .emika_ft_estimator import EmikaFTEstimator
