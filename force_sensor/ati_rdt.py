# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2017"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"

import threading
import socket
import enum
import struct

import numpy as np

from . import ForceTorqueI, Modes



class ATIRDT(ForceTorqueI):
    """Class for sampling the ATI force sensor over UDP+RDT in both single
    shot and streaming mode.
    """

    REQ_STRUCT = struct.Struct('>HHI')
    FT_STRUCT = struct.Struct('>IIIiiiiii')

    def __init__(self, rdt_host, rdt_port=49152, bind_host='0.0.0.0'):
        ForceTorqueI.__init__(self)
        self.daemon = True
        self._rdt_addr = (rdt_host, rdt_port)
        self._bind_addr = (bind_host, rdt_port)
        self._sock = socket.socket(type=socket.SOCK_DGRAM)
        self._sock.settimeout(0.005)
        self._sock.bind(self._bind_addr)
        self._cycle_time = 0.0
        self._enc_to_si = np.array(6 * [1.0e-6])

    def start_stream(self):
        if self._mode == Modes.STREAM:
            # Already in streaming mode
            return False
        self._sock.sendto(ATIRDT.REQ_STRUCT.pack(0x1234, 2, 0),
                          self._rdt_addr)
        self._mode = Modes.STREAM
        self._stream_flag.set()
        return True

    def stop_stream(self):
        if self._mode == Modes.SINGLE:
            # Already in single mode
            return False
        self._stream_flag.clear()
        self._sock.sendto(ATIRDT.REQ_STRUCT.pack(0x1234, 0, 0),
                          self._rdt_addr)
        self._mode = Modes.SINGLE
        # Clear the receive buffer
        while self._latest_ft is not None:
            self._recv_ft()
        return True

    def _recv_ft(self):
        try:
            data = ATIRDT.FT_STRUCT.unpack(self._sock.recv(1024))
        except socket.timeout:
            with self._ft_lock:
                self._latest_ft = None
        else:
            with self._ft_lock:
                self._latest_ft = self._enc_to_si * np.array(data[3:])

    def _req_single_ft(self):
        self._sock.sendto(ATIRDT.REQ_STRUCT.pack(0x1234, 2, 1),
                          self._rdt_addr)
        self._recv_ft()
