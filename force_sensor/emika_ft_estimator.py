# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2018"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"

import threading
import socket
import struct

import numpy as np

from  . import ForceTorqueI, Modes

class EmikaFTEstimator(ForceTorqueI):
    def __init__(self, emika_gw_host, emika_gw_port=10000, bind_host='0.0.0.0'):
        ForceTorqueI.__init__(self)
        self._emika_gw_addr = (emika_gw_host, emika_gw_port)
        self._bind_addr = (bind_host, emika_gw_port)
        self._sock = socket.socket(type=socket.SOCK_DGRAM)
        self._sock.settimeout(0.015)
        self._sock.bind(self._bind_addr)
        self._w_struct = struct.Struct('6d')
        self.start_stream()

    def _recv_ft(self):
        try:
            # Note that Emika gives the FT which it exerts, whereas a
            # FT sensor is expected to tell the FT which it
            # experiences. Hence the minus sign.
            new_ft = -np.array(self._w_struct.unpack(self._sock.recv(1024)))
            with self._ft_lock:
                self._latest_ft = new_ft
        except socket.timeout:
            print('Warning: Timeout during reception of data.')
            with self._ft_lock:
                self._latest_ft = None

    def _req_single_ft(self):
        self.wait_for_data()
